import React, { Component } from 'react';
import { Grid, Cell,Chip } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import TimelineItem from './TimelineItem';
import Skills from './skills';
import './extra.css';

class Resume extends Component {
  render() {
    return(
      <div>
        <Grid className="mdl-color--grey-100">
          <Cell col={4}>
            <div style={{textAlign: 'center'}}>
              <img
                src="https://yt3.ggpht.com/a/AGF-l7_oGwiTUh_Y36fio5QY0fuZPtihhIZFvEE_pA=s288-mo-c-c0xffffffff-rj-k-no"
                alt="avatar"
                style={{height: '200px'}}
                 />
            </div>

            <h2 style={{paddingTop: '1em'}}>Peter Hassaballah</h2>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h6 style={{color: 'grey'}}>Certificates</h6>
            <p>DELF B1 </p>
            <p>Kythara institue for music </p>
            <p>egFWD: Advanced Data Analysis Course (Ongoing) </p>
            <h6 style={{color: 'grey'}}>Pulications</h6>
            <a href="https://ieeexplore.ieee.org/document/9068106">
              IEEE: pyDAW: A Pragmatic CLI for Digital Audio Processing </a>
            
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h6 style={{color: 'grey'}}>Extra Curricular Activities</h6>
            <b> XOP GUC </b>
            <p> Vice President </p>
            <b> GUC CC </b>
            <p> Curricullum Committe member in MET faculty from 2016 to 2018</p>
            <b> Junior TA </b>
            <p> Junior TA for CSEN 202 Spring 2017</p>
            <b> GUC MUN </b>
            <p> Delegate in 2016 &amp; 2017</p>
            <b> TCS-GUC </b>
            <p> Media Team</p>
            <b> Ihlam-GUC </b>
            <p> Media Team</p>
            <b> Inspire </b>
            <p> Logistics and organization Team</p>
      
            
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h5>Address</h5>
            <p>Nasr City , Cairo , Egypt </p>
            <h5>Phone</h5>
            <p>012 77 915 915</p>
            <h5>Email</h5>
            <p>peter.hassaballah@gmail.com</p>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h2>Soft Skills</h2>
                
                <Skills skill="Problem Solving"  progress={95} />
              
                <Skills skill="Decision Making" progress={75} />
                <Skills skill="Leadrship" progress={85} />
                <Skills skill="Management"  progress={85} />
                
                <Skills skill="Research"progress={95} />
                
                <Skills skill="Enterprise Resource Planning (ERP)" progress={45} />
                <Skills skill="Analysis" progress={85} />
                
                <Skills skill="Teaching" progress={75} />
                
                <Skills skill="Customer Relationship Management (CRM)"progress={55} />
                <h4> Languages</h4>
                <Skills skill="English"progress={100} />
                <Skills skill="Arabic"progress={100} />
                <Skills skill="French"progress={50} />
                <Skills skill="Deutsch"progress={25} />
                
              {/* <hr style={{borderTop: '3px solid #e22947'}} /> */}
              
            
          </Cell>
          <Cell className="resume-right-col" col={8}>
            <h2>Education</h2>


            <Education
              startYear={2007}
              endYear={2015}
              schoolName="New Ramses College (NRC)"
              schoolDescription="IGCSE Class of '15 "
               />

               <Education
                 startYear={2015}
                 endYear={2020}
                 schoolName="German Uninversity in Cairo (GUC)"
                 schoolDescription="Faculty of Engineering. Digital Media Engineering Technology (DMET) "
                  />
                <hr style={{borderTop: '3px solid #e22947'}} />

              <h2>Experience</h2>
              
      <div className="vertical-timeline">
        
      <TimelineItem title="Before Grad">
        
      <TimelineItem title="2015">
            <Experience
                startYear={"June 2015"}
                endYear={" Sept 2016"}
                jobName="Petro Establishment"
                jobInfo="Technical Sales for a heavy equipment Retail Company"
                jobDescription= {[
                  "Managing Database systems ",
                  "Items Querying and Data Entry"
                ]}
              />
          </TimelineItem>
      <TimelineItem title="2016">

              <Experience
                startYear={"June 2016"}
                endYear={" August 2016"}
                jobName="Teleperformance"
                jobInfo=" Western Union UK account "
                jobDescription={[
                  "1st Line Technical Support",
                  "Troubleshooting technical errors during transactions"
                ]}
                />
                </TimelineItem>
      
      <TimelineItem title="2018-2020">
        
      <Experience
                startYear={"March 2018"}
                endYear={" April 2019"}
                jobName="dotmeta"
                jobInfo="Fullstack Developer"
                jobDescription={[
                  "Implemented a Media WebApp using Django",
                  "Created a RESTful API for the website",
                  "Created a new video platform tool using pipelines from AWS and FFMPEG commands",
                  "Designed a graphical object canvas"
                ]}
                />
                 <Experience
                startYear={"September 2018"}
                endYear={" April 2019"}
                jobName="Qpix"
                jobInfo="Application Developer"
                jobDescription={[
                  "Developing a WebApp using MERN stack",
                  "Developed a react native Game application",
                  "Integrated socket programming connections",
                  "Created a new video streaming technique using a nginx server and RTMP protocol"
                ]}
                />
               
                 <Experience
                startYear={"June 2019"}
                endYear={" October 2019"}
                jobName="InstaCure"
                jobInfo="Full Stack Techincal Lead"
                jobDescription={[
                  "Developing a Medical WebApp using MERN stack",
                  "Developed a Video conferencing service"
                ]}
                />

                 <Experience
                startYear={"Novermber 2019"}
                endYear={"October 2020"}
                jobName="Bamboo Orchard"
                jobInfo="Software Engineer"
                jobDescription={[
                  "Developing a Quiz WebApp using MERN stack",
                  "Developing a Dashboard for a WebApp using react.js,redux,saga"
                ]}
                />
      </TimelineItem>
      
      </TimelineItem>
            </div>


                <Experience
                startYear={"December 2020"}
                endYear={"October 2021"}
                jobName="Pixelogic Media"
                jobInfo="Data Specialist"
                jobDescription={[
                  "Providing support to departments such as Digital Cinema, Localization, Digital Mastering",
                  "Writing Scripts to Automate processes and daily workflows"
                  
                ]}
                />    
                 <Experience
                startYear={"July 2020"}
                endYear={"Jan 2023"}
                jobName="Out Of The Box Soulutions"
                jobInfo="Founder"
                jobDescription={[
                  "Offering uniquely tailored, creative software solutions",
                  "Empowering our partners and transform how they engage their users by providing innovative and engaging digital services.",
                  "As a Project Manager I organize the roadmap (Technically) using gantt charts and trello boards ",
                  "I manage the dev. team to ensure adherence to business requirements and UI design specifications and create stories and iterations upon.",
                  "I also communicate key insights and findings to product team"
                ]}
                /> 
                <Experience
                startYear={"April 2022"}
                endYear={"Jan 2023"}
                jobName="Fresh Square"
                jobInfo="Co-Founder"
                jobDescription={[
                  "As a Technology Lead I developed the entire API and the admin dashboard system",
                  "I provide backlog management, iteration planning, and elaboration of the user stories ",
                  "As a Project Manager I communicate with both marketing and operations teams about our milestones targets ",
                  "I partake in critical decisions related to marketing strategies and milestones prioritization",
                  "I manage the dev. team to ensure that there is always an adequate amount of prior prepared tasks to work on",
                  "I work closely with the product team on most details as I have sufficient knowledge and background regarding the needs of the project"
                ]}
                />
                 <Experience
                startYear={"November 2022"}
                endYear={"December 2023"}
                jobName="Kashier Payment Solutions"
                jobInfo="Backend Engineer"
                jobDescription={[
                  "Improved performance of microservices by applying custom dockerconfigurations.",
                  "Modernized Fintech solutions by refactoring legacy code by rewriting some in TypeScript and revamping it using current industry trends",
                  "Built and documented new features and APIs ( MEAN stack ) and support frontend engineers with API integrations",
                ]}
                /> 
                <Experience
                startYear={"November 2021"}
                endYear={"Present"}
                jobName="Mashy Live"
                jobInfo="Co-Founder"
                jobDescription={[
                  "As a Technology Lead I overview all choice of technology and tools to be used to best service the Website",
                  "I also review, analyze and evaluate user stories to suit our user needs.",
                  "Participating with stakeholders on different fronts (business, product and tech) to enhance our product and team",
                  "Responsible for leadership and architecture of all of our software tools and portals. (MERN stack, next.js & AWS)"
                  
                ]}
                /> 
                
                <hr style={{borderTop: '3px solid #e22947'}} />
              <h2>Tools and Frameworks</h2>
              <h3>Proficient:</h3>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">React </Chip>
              <Chip className="mdl-color--amber mdl-color-text--white m-4">Python </Chip>
              <Chip className="mdl-color--deep-purple mdl-color-text--white m-4">ffmpeg </Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">Netlify </Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">NodeJS </Chip>
              <Chip className="mdl-color--amber mdl-color-text--white m-4">ExpressJS </Chip>
              <Chip className="mdl-color--deep-purple mdl-color-text--white m-4">Mongoose </Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">TypeScript </Chip>

              <h3>Familiar:</h3>
              <Chip className="mdl-color--indigo mdl-color-text--white m-4">Django </Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">Java </Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">Google Cloud Platform (GCP) </Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">AWS </Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">Bash</Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">pandas</Chip>
              <Chip className="mdl-color--indigo mdl-color-text--white m-4">Matlab</Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">prolog</Chip>
              <Chip className="mdl-color--indigo mdl-color-text--white m-4">C</Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">C++</Chip>
              <Chip className="mdl-color--orange mdl-color-text--white m-4">lodash</Chip>
              <Chip className="mdl-color--indigo mdl-color-text--white m-4">haskell</Chip>
              <Chip className="mdl-color--deep-orange mdl-color-text--white m-4">redis</Chip>
              <Chip className="mdl-color--indigo mdl-color-text--white m-4">next.js</Chip>
            
              
                
              
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Resume;
