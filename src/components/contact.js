import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent } from 'react-mdl';


class Contact extends Component {
  render() {
    return(
      <div className="contact-body">
        <Grid className="contact-grid">
          <Cell col={6}>
            <h2>Peter Hassaballah</h2>
            <img
              src="https://yt3.ggpht.com/a/AGF-l7_oGwiTUh_Y36fio5QY0fuZPtihhIZFvEE_pA=s288-mo-c-c0xffffffff-rj-k-no"
              alt="avatar"
              style={{height: '250px'}}
               />
             <p style={{ width: '75%', margin: 'auto', paddingTop: '1em'}}> TO CONTACT ME USE ANY OF THESE CHANNELS I AM FRIENDLY AS LONG AS THE SUN IS UP :) </p>

          </Cell>
          <Cell col={6}>
            <h2>Contact Me</h2>
            <hr/>

            <div className="contact-list">
              <List>
                <ListItem>
                  <ListItemContent style={{fontSize: '30px',textAlign:'right', fontFamily: 'Anton'}}>
                    
                    <Grid>
                      <Cell col={2} style={{textAlign:'left'}}>
                        <i className="fa fa-phone-square" aria-hidden="true"/>
                      </Cell>
                      <Cell col={10}>
                        +20 (012) 77-915-915
                      </Cell>
                    </Grid>
                  </ListItemContent>
                </ListItem>


                <ListItem>
                  <ListItemContent style={{fontSize: '30px',textAlign:'right',  fontFamily: 'Anton'}}>
                    <Grid>
                      <Cell col={2} style={{textAlign:'left'}}>
                        <i className="fa fa-envelope" aria-hidden="true"/>
                      </Cell>
                      <Cell col={10}>
                        peter.hassaballah@gmail.com
                      </Cell>
                    </Grid>
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '30px',textAlign:'right',  fontFamily: 'Anton'}}>
                    <Grid>
                      <Cell col={2} style={{textAlign:'left'}}>
                        <i className="fa fa-linkedin" aria-hidden="true"/>
                      </Cell>
                      <Cell col={10}>
                        Peter Hassaballah 
                      </Cell>
                    </Grid>
                  </ListItemContent>
                </ListItem>


              </List>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Contact;
