import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class Landing extends Component {
  render() {
    return(
      <div style={{width: '100%', margin: 'auto'}}>
        <Grid className="landing-grid">
          <Cell col={12}>
            <img
              src="https://yt3.ggpht.com/a/AGF-l7_oGwiTUh_Y36fio5QY0fuZPtihhIZFvEE_pA=s288-mo-c-c0xffffffff-rj-k-no"
              alt="avatar"
              className="avatar-img"
              />

            <div className="banner-text">
              <h1>Software Engineer </h1>

            <hr/>

          {/* <p> Django | Express | JavaScript | MongoDB | NodeJS | php | React | React Native | SQL </p> */}

        <div className="social-links">

          {/* LinkedIn */}
          <a href="https://www.linkedin.com/in/peter-hassaballah/" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-linkedin-square" aria-hidden="true" />
          </a>

          {/* Github */}
          <a href="https://github.com/PeterHassaballah" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-github-square" aria-hidden="true" />
          </a>

          {/* Stack Over Flor */}
          <a href="https://stackoverflow.com/users/7219733/peter-hassaballah" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-stack-overflow" aria-hidden="true" />
          </a>

          {/* Gitlab */}
          <a href="https://gitlab.com/peterHassaballah?nav_source=navbar" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-gitlab" aria-hidden="true" />
          </a>

          {/* Behance */}
          <a href="https://www.behance.net/peterhassa72c3" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-behance" aria-hidden="true" />
          </a>
        </div>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Landing;
