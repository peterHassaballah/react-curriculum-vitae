import React, { Component } from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu  } from 'react-mdl';


class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0 };
  }

  toggleCategories() {

    if(this.state.activeTab === 0){
      return(
        <div className="projects-grid">
          {/* Project 1 */}
          <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
            <CardTitle style={{ height: '176px', background: 'url(https://1000logos.net/wp-content/uploads/2019/03/Logo-IEEE-500x281.jpg) center / cover'}} > <b>py</b>DAW</CardTitle>
            <CardText>
              ICCES conference
            </CardText>
            
            <CardActions border>
              <Button href="https://www.researchgate.net/publication/340690366_pyDAW_A_Pragmatic_CLI_for_Digital_Audio_Processing" colored>Preview</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>

          {/* Project 2 */}
          {/* <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
            <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover'}} >React Project #2</CardTitle>
            <CardText>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>CodePen</Button>
              <Button colored>Live Demo</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card> */}

          {/* Project 3 */}
          {/* <Card shadow={5} style={{minWidth: '450', margin: 'auto'}}>
            <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://xtnotes-1255646395.coshk.myqcloud.com/images/react-1.svg) center / cover'}} >React Project #3</CardTitle>
            <CardText>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
            </CardText>
            <CardActions border>
              <Button colored>GitHub</Button>
              <Button colored>CodePen</Button>
              <Button colored>Live Demo</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
        */}
        </div>


      )
    } else if(this.state.activeTab === 1) {
      return (
        <div><h1>References Upon Request</h1>
          <div className="projects-grid">
        <Card shadow={5} style={{minWidth: '150', margin: 'auto'}}>
            <CardTitle style={{ height: '176px', background: 'url(https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fis2.mzstatic.com%2Fimage%2Fthumb%2FPurple128%2Fv4%2Fc8%2Fb6%2Fc3%2Fc8b6c313-c3c1-1463-b083-c47c428abc5d%2Fsource%2F1200x630bb.jpg&f=1) center / cover'}} >Java</CardTitle>
            <CardText>
              Harry Potter Board Game
            </CardText>
            <CardActions border>
              <Button href="https://drive.google.com/open?id=1koDO343-D_CKW4rgww_RJ8aUB6AGfjPq" target="_blank"  colored>Download</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>

          <Card shadow={5} style={{minWidth: '150', margin: 'auto'}}>
            <CardTitle style={{ height: '176px', background: 'url(https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fis2.mzstatic.com%2Fimage%2Fthumb%2FPurple128%2Fv4%2Fc8%2Fb6%2Fc3%2Fc8b6c313-c3c1-1463-b083-c47c428abc5d%2Fsource%2F1200x630bb.jpg&f=1) center / cover'}} >Java</CardTitle>
            <CardText>
              Web Socket Chat application
            </CardText>
            <CardActions border>
              <Button href="https://github.com/PeterHassaballah/UDP-JAVA-Audio-Chat" target="_blank"  colored>GitHub</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>

          <Card shadow={0} style={{minWidth: '150', margin: 'auto'}}>
            <CardTitle style={{ height: '176px', background: 'url(https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fis2.mzstatic.com%2Fimage%2Fthumb%2FPurple128%2Fv4%2Fc8%2Fb6%2Fc3%2Fc8b6c313-c3c1-1463-b083-c47c428abc5d%2Fsource%2F1200x630bb.jpg&f=1) center / cover'}} >C++ &amp; Open GL</CardTitle>
            <CardText>
              Medal of Honour Zombie Mode
            </CardText>
            <CardActions border>
              <Button href="#" colored>Download</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>

          <Card shadow={0} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{ height: '176px', background: 'url(https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fis2.mzstatic.com%2Fimage%2Fthumb%2FPurple128%2Fv4%2Fc8%2Fb6%2Fc3%2Fc8b6c313-c3c1-1463-b083-c47c428abc5d%2Fsource%2F1200x630bb.jpg&f=1) center / cover'}} > Arduino &amp; FPGA </CardTitle>
            <CardText>
              Smart Parking System
            </CardText>
            <CardActions border>
              <Button href="https://www.youtube.com/watch?v=-Xc0yw9cYPM" target="_blank"  colored>Watch</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
          
        </div>
        
        </div>
      )
    } else if(this.state.activeTab === 2) {
      return (
        <div className="projects-grid">
          {/* <h1>References Upon Request</h1> */}
          
          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{ color:'#fff' ,height: '176px', background: 'url(https://reactjsexample.com/content/images/2016/08/20160828182925.jpg) center / cover'}} > Mongo </CardTitle>
            <CardText>
              Socket.io Chat app
            </CardText>
            <CardActions border>
              <Button href="https://github.com/qpixHassaballah/ChatMongoose" target="_blank" colored>Repo</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>

          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{color:'#fff', height: '176px', background: 'url(https://reactjsexample.com/content/images/2016/08/20160828182925.jpg) center / cover'}} > React Native </CardTitle>
            <CardText>
              Trivia App
            </CardText>
            <CardActions border>
              <Button href="https://www.behance.net/gallery/67659967/Trivia" target="_blank"  colored>Wireframe</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{color:'', height: '176px', background: 'url(https://pypi.org/static/images/logo-small.95de8436.svg) center / cover'}} > Python</CardTitle>
            <CardText>
              pyDAW a CLI tool
            </CardText>
            <CardActions border>
              <Button href="https://pypi.org/project/pydaw/" target="_blank"  colored>Link</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{color:'', height: '176px', background: 'url(https://user-assets.sharetribe.com/images/communities/wide_logos/141637/header/logotrans.png?1646496220) center / cover'}} > </CardTitle>
            <CardText>
              Mashy
            </CardText>
            <CardActions border>
              <Button href="https://www.mashy.live" target="_blank"  colored>Link</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{color:'', height: '176px', background: 'url(https://i.ibb.co/bN65PD6/solidbg-1.png) center / cover'}} ></CardTitle>
            <CardText>
              Solid
            </CardText>
            <CardActions border>
              <Button href="https://www.solidgroupeg.com/" target="_blank"  colored>Link</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
          <Card shadow={5} style={{minWidth: '150',  margin: 'auto'}}>
            <CardTitle style={{color:'', height: '176px', background: 'url(https://freshsq.com/images/cc.svg) center / cover'}} ></CardTitle>
            <CardText>
              Fresh Square
            </CardText>
            <CardActions border>
              <Button href="https://freshsq.com/" target="_blank"  colored>Link</Button>
            </CardActions>
            <CardMenu style={{color: '#fff'}}>
                
            </CardMenu>
          </Card>
        </div>
      )
    } 

  }



  render() {
    return(
      <div>
        <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
          <Tab>Research</Tab>
          <Tab>GUC Projects</Tab>
          <Tab>Personal Projects</Tab>
        </Tabs>


          <Grid>
            <Cell col={12}>
              <div className="content">{this.toggleCategories()}</div>
            </Cell>
          </Grid>


      </div>
    )
  }
}

export default Projects;
