import React, { Component } from 'react';

import { FABButton,Icon } from 'react-mdl';
import './extra.css';

class TimelineItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  toggleExpanded = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded
    }));
  };

  render() {
    const { title, children } = this.props;
    const { expanded } = this.state;

    return (
      <div className="timeline-item">
        <div className="timeline-content">
          <h3>{title}</h3>
          <FABButton className="mdl-color-text--white" ripple onClick={this.toggleExpanded}>{expanded ? 
    '-' : 
    <Icon name="add" />}</FABButton>
          {expanded && children && <div className="branch">{children}</div>}
        </div>
      </div>
    );
  }
}

export default TimelineItem;
