import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

class Experience extends Component {
  constructor(props){
    super(props);
    this.state={
      list:[],
    }
  }
  componentDidMount(){
    // console.log(this.props.jobDescription);
    this.setState({list:this.props.jobDescription});

  }
  render() {
    return(
      <Grid>
        <Cell col={4}>
          <p>{this.props.startYear} - {this.props.endYear}</p>
        </Cell>
        <Cell col={8}>
          <h4 style={{marginTop:'0px'}}>{this.props.jobName}</h4>
          <p> {this.props.jobInfo} </p>
          <ul>
            {this.state.list.map((item, index) =>
          <li key={index}>
            {item}
          </li>)}
          </ul>
        </Cell>
      </Grid>
    )
  }
}

export default Experience;
