import React, { Component } from 'react';
import { Grid, Card, CardText,CardTitle, CardActions, Button, CardMenu, IconButton } from 'react-mdl';
import './about.css'; // Import your CSS file for styling

class About extends Component {
  render() {
    return(
      <div className="about-page">
        <Grid className="mdl-color--grey-100">
          <div className="about-content">
            <h2>About Me</h2>
            <div className="about-paragraph">
              <p>
                A musician at heart, I like photography, yoga, and crossFit.
                Active on Stack Overflow for moderating and on Twitter for ranting.
                Infamous owner of multiple side projects that haven't seen the light yet.
              </p>
            </div>
            <div style={{padding:'12px'}}>
              <h3> A summary can be found in the PDF format of my Curriculum Vitae </h3>
              <div className="projects-grid">
                {/* Project 1 */}
                <Card shadow={5} className="cv-card">
                <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://png.pngtree.com/element_our/png/20181008/cv-logo-template-icon-isolated-on-black-background-png_130993.jpg) center / cover'}} >My CV</CardTitle>
                  <CardText>
                    CV PDF format
                  </CardText>
                  <CardActions border>
                    <Button href="https://drive.google.com/file/d/1ATh8mNibqNN2Ei6V50mxYt5kTAoe3Npv/view?usp=drive_link" colored>View</Button>
                  </CardActions>
                  <CardMenu style={{color: '#fff'}}>
                    <IconButton name="share" />
                  </CardMenu>
                </Card>
              </div>
            </div>
          </div>
        </Grid>
      </div>
    );
  }
}

export default About;
